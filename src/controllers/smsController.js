'use strict'
const util = require('util')
const appConf = require('../../conf/application')
const log = require('../logger')
const redisConf = require('../../conf/redis')

//create seperate file to create client
const redis = require('redis')
const client = redis.createClient(redisConf.PORT, redisConf.HOST,
{
    'auth_pass': redisConf.AUTH_KEY,
    'return_buffers': true
  }
).on('error', (err) => log.error('ERR:REDIS:', err));

const {findMissingRequiredParams, findInvalidParams} = require('../utils/sms')

function inbound(req, res) {
    try{
        log.info('start of inbound reqesut')
        let data = req.body
        // check all params are present, if present validate else return missing param and corresponding param
        let missingParams = findMissingRequiredParams(data)
        if(missingParams.length>0){
            log.error(util.format('%s is missing', missingParams[0]))
            res.send({message:'', error:util.format('%s is missing', missingParams[0])})
        }
        let invalidParams = findInvalidParams(data)
        if(invalidParams.length>0){
            log.error(util.format('%s is invalid', invalidParams[0]))
            res.send({message:'', error:util.format('%s is invalid', invalidParams[0])})
        }
        else{
            let trimmedText = data['text'].trim().toLocaleLowerCase()
            if(appConf.STOP_WORDS.indexOf(trimmedText)>=0){
                client.set(data['from']+'#'+data['to'], true, 'EX', redisConf.PAIR_TTL)
            }
            log.info('inbound sms is ok')
            res.send({message:'inbound sms is ok', error:''})
        }
    }
    catch(err){
        log.error('unknown failure',err);
        res.send({message:'', error:'unknown failure'})
    }

}

function outbound(req, res){
    try{
        log.info('start of outbound')
        let data = req.body
        // check all params are present, if present validate else return missing param and corresponding param
        let missingParams = findMissingRequiredParams(data)
        if(missingParams.length>0){
            log.error(util.format('%s is missing', missingParams[0]))
            res.send({message:'', error:util.format('%s is missing', missingParams[0])})
        }
        let invalidParams = findInvalidParams(data)
        if(invalidParams.length>0){
            log.error(util.format('%s is invalid', invalidParams[0]))
            res.send({message:'', error:util.format('%s is invalid', invalidParams[0])})
        }
        else{
            let from = data['from']
            let to = data['to']
            let trimmedText = data['text'].trim().toLocaleLowerCase()
            let cacheKey = from+'#'+to
            log.info('start of getting value for key: ', cacheKey)
            client.get(cacheKey, function(error, result) {
                if (error) {
                    log.info('Error in redis.get for key: ', cacheKey)
                    throw error;
                }
                else {
                    if(result){
                        log.info('SMS is blocked by STOP request', result)
                        res.send({message: '', error: 'sms is blocked by STOP request'})
                    }
                    else{
                        client.get(from, function(error, result) {
                            let message ='outbound sms is ok'
                            let err = ''
                            if(error){
                                log.error('Error from cache layer', error)
                                message =''
                                err = 'unknown failure'
                            }
                            else{
                                if (!result){
                                    client.set(from, 1, 'EX', redisConf.FROM_TTL)
                                }else if (result<redisConf.MAX_REQUESTS_IN_HOUR){
                                    client.incr(from)
                                } else{
                                    log.info('Exceeds number of outbound requests from', from)
                                    message = ''
                                    err =util.format('Limit reached for from %s', from)
                                }
                            }
                            res.send({message: message, error:err})
                        })
                    }
                }
            })
        }
    }
    catch(error){
        let message =''
        let err = 'unknown failure'
        log.error('Error in catch', error)
        res.send({message:message, error:err})
    }
}

module.exports = {
    inboundController: inbound,
    outboundController: outbound
}
