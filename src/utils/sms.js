'use strict'
const util = require('util')
const appConf = require('../../conf/application')
const log = require('../logger')

const requiredParams = appConf.REQUIRED_PARAMS
let requiredParamsValidationMap = appConf.REQUIRED_PARAM_VALIDATION_MAP
let numbers = appConf.NUMBERS
let strings = appConf.STRINGS
let stopWords = appConf.STOP_WORDS

function findMissingRequiredParams(data){
    let missingParams = []
    for(let param of requiredParams){
        if(!(param in data)){
            missingParams.push(param)
        }
    }
    return missingParams
}
function findInvalidParams(data){
    let invalidParams = []
    for(let param of requiredParams){
        let value = data[param]
        if(!value || !(isValid(param, value))){
            invalidParams.push(param)
        }
    }
    return invalidParams
}

function isValid(param, value){
    let paramLimit = requiredParamsValidationMap[param]
    log.info(util.format('start of validating param:%s with value: %s', param, value))
    if(numbers.indexOf(param)>=0){
        log.info('lenght of value is ', value.toString().length)
        let valLength = value.toString().length
        log.info('length of',param, valLength)
        if (paramLimit[0]<=valLength && paramLimit[1]>=valLength) return true
    }
    else if(strings.indexOf(param)>=0){
        let valLength = value.length
        log.info('length of',param, valLength)
        if (paramLimit[0]<=valLength && paramLimit[1]>=valLength) return true
    }
    else{
        //other cases
        return false
    }
    return false
}

module.exports = {
    findInvalidParams,
    findMissingRequiredParams
}