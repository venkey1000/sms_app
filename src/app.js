'use strict';
const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./routes')
const app = express()

const log = require('./logger')

app.use(bodyParser.json())
app.use('/', routes)
app.listen(process.env.PORT||5000, () => log.info('Example app listening on port 3000!'))

module.exports = app
