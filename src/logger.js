'use strict'
var bunyan = require('bunyan');
var log = bunyan.createLogger({name: "sms.log",
    streams: [{
        // `type: 'file'` is implied
      stream: process.stdout, level:'info'
    }]
});


module.exports = log
