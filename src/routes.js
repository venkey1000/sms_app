'use strict'
const express  = require('express')
const router = express.Router()
const {inboundController, outboundController} = require('./controllers/smsController')

router.post('/inbound/sms', inboundController)
router.post('/outbound/sms', outboundController)

router.use(function(req, res, next) {
    res.status(404).send({'error':'Not Found'});

});

module.exports = router

