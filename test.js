'use strict';

const chai = require('chai');
const expect = require('chai').expect;

chai.use(require('chai-http'));

const app = require('./src/app.js'); // Our app
const util = require('util');

describe('API endpoint /inbound/sms', function() {
    this.timeout(5000); // How long to wait for a response (ms)

    before(function() {

    });

    after(function() {

    });


    // GET - Invalid path
    it('should return 404', function() {
        return chai.request(app)
            .get('/INVALID_PATH')
            .then(function(res) {
                expect(res).to.have.status(404);
            })
    });

    // POST - send inbound sms OK
    it('send new inbound sms', function() {
        return chai.request(app)
            .post('/inbound/sms')
            .send({
                from: '012345',
                to: '556789',
                text: 'stop'
            })
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.eql({message :'inbound sms is ok',
                    error :'' });
            });
    });

    //POST - inbound sms field invalid
    it('should throw error with field is missing', function() {
        return chai.request(app)
            .post('/inbound/sms')
            .send({
                to: '556789',
                text: 'abc'
            })
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.eql({message :'',
                    error :'from is missing'});
            });
    });

    //POST- inbound sms field invalid
    it('should throw error with field is invalid', function() {
        return chai.request(app)
            .post('/inbound/sms')
            .send({
                from:'012345',
                to: '556789',
                text: ''
            })
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.eql({message :'',
                    error :'text is invalid'});
            });
    });

});

describe('API endpoint /outbound/sms', function() {
    this.timeout(5000); // How long to wait for a response (ms)

    before(function() {

    });

    after(function() {

    });


    // POST - block outbound sms
    it('send new outbound sms', function() {
        return chai.request(app)
            .post('/outbound/sms')
            .send({
                from: '012345',
                to: '556789',
                text: 'anc'
            })
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.eql({message :'',
                    error :'sms is blocked by STOP request' });
            });
    });

    //POST -  outbound sms is okay
    it('send new outbound sms', function() {
        return chai.request(app)
            .post('/outbound/sms')
            .send({
                from: '012345',
                to: '556784',
                text: 'anc'
            })
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.eql({message :'outbound sms is ok',
                    error :'' });
            });
    });

    //POST -  outbound sms throw error
    it('should throw error with field is invalid', function() {
        return chai.request(app)
            .post('/outbound/sms')
            .send({
                from:'012345',
                to: '556789',
                text: ''
            })
            .then(function(res) {
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body).to.eql({message :'',
                    error :'text is invalid'});
            });
    });
});